#ifndef UNIT_TEST_STORAGE_UPDATES_H
#define UNIT_TEST_STORAGE_UPDATES_H

#include <unit_tests.h>
#include <storages.h>

class UnitTestStorageUpdates : public UnitTestBase {
    public:
        UnitTestStorageUpdates();
        virtual void init();
        virtual bool run();

    private:
        
};

#endif // UNIT_TEST_STORAGE_UPDATES_H
